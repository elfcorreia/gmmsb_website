# Generated by Django 2.0.9 on 2018-10-17 13:37

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Publication',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.TextField()),
                ('doi', models.CharField(max_length=50)),
                ('date', models.DateField()),
            ],
        ),
    ]
