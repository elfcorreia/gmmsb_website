# Generated by Django 2.0.9 on 2018-10-17 13:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grupo', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='publication',
            name='date',
            field=models.DateField(auto_now_add=True),
        ),
    ]
