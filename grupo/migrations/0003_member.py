# Generated by Django 2.0.9 on 2018-10-17 14:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('grupo', '0002_auto_20181017_1347'),
    ]

    operations = [
        migrations.CreateModel(
            name='Member',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('title', models.CharField(max_length=255)),
                ('avatar_url', models.CharField(max_length=255)),
                ('lattes_url', models.CharField(max_length=255)),
            ],
        ),
    ]
