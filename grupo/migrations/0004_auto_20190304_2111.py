# Generated by Django 2.1.1 on 2019-03-04 21:11

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('grupo', '0003_member'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='member',
            options={'ordering': ['name']},
        ),
        migrations.AlterModelOptions(
            name='publication',
            options={'ordering': ['-date']},
        ),
    ]
