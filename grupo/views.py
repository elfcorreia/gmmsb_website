from django.shortcuts import render
from . import models
from content.views import page_view

def home(request):
	return page_view(request, slug='home')

def events(request):
	ctx = {}
	return render(request, "grupo/events.html", ctx, content_type='text/html')

def research(request):	
	ctx = {}
	return render(request, "grupo/research.html", ctx, content_type='text/html')

def publications(request):	
	ctx = {
		"publications": models.Publication.objects.all(),
	}
	return render(request, "grupo/publications.html", ctx, content_type='text/html')

def services(request):
	ctx = {}
	return render(request, "grupo/services.html", ctx, content_type='text/html')

def team(request):
	ctx = {
		"members": models.Member.objects.all(),
	}
	return render(request, "grupo/team.html", ctx, content_type='text/html')

def contact(request):
	ctx = {}
	return render(request, "grupo/contact.html", ctx, content_type='text/html')