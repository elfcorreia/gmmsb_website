from django.contrib import admin
from . import models

class MyAdminSite(admin.AdminSite):
	site_header = 'GMMSB administration'

@admin.register(models.Publication)
class PublicationAdmin(admin.ModelAdmin):
	list_display = ('description', 'doi', 'date')
	date_hierarchy = 'date'

@admin.register(models.Member)
class MemberAdmin(admin.ModelAdmin):
	list_display = ('name', 'title', 'lattes_url')	