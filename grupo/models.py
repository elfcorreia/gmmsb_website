from django.db import models

# Create your models here.

class Publication(models.Model):
	description = models.TextField()
	doi = models.CharField(max_length=50)
	date = models.DateField(auto_now_add=True)

	def __str__(self):
		return self.description[:100] + "..."

	class Meta:
		ordering = ["-date"]

class Member(models.Model):
	name = models.CharField(max_length=255)
	title = models.CharField(max_length=255)
	avatar_url = models.CharField(max_length=255)
	lattes_url = models.CharField(max_length=255)

	class Meta:
		ordering = ["name"]