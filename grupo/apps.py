from django.apps import AppConfig
from django.contrib.admin.apps import AdminConfig

class GrupoConfig(AppConfig):
    name = 'grupo'

class MyAdminConfig(AdminConfig):
    default_site = 'grupo.admin.MyAdminSite'