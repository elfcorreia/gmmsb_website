from django.apps import AppConfig


class PortaisConfig(AppConfig):
    name = 'portais'
