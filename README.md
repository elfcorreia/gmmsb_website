# gmmsb_website

Web site of GMMSB research group

# Installation

1. Create a python3 virtualenv
	`virtualenv ENV`

2. Activate the virtualenv
	`source ENV/bin/activate`

3. Install python dependencies
	`pip install -r requirements.txt`

# Development Server

1. Activate the virtualenv
	`source ENV/bin/activate`

2. Start django built-in server
	`./manage runserver`