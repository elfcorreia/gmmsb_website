from django.shortcuts import render
from . import models

def page_view(request, slug):
	try:
		page = models.Page.objects.get(slug=slug) 
	except models.Page.DoesNotExist as e:
		return render(request, "content/page-not-found.html", content_type="text/html")
		
	if not page.visible:
		return render(request, "content/page-not-found.html", content_type="text/html")
		
	ctx = {
		'page': page
	}
	return render(request, "content/page.html", ctx, content_type='text/html')