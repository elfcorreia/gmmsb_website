from . import models

def nav_bar_items(request):
	return { 'nav_bar_items': models.HomeNavBarItem.objects.all() }