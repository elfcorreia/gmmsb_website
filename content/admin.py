from django.contrib import admin
from . import models

@admin.register(models.Page)
class PageAdmin(admin.ModelAdmin):
	list_display = ('slug', 'title', 'visible')

@admin.register(models.Home)
class HomeAdmin(admin.ModelAdmin):	

	def has_add_permission(self, request):
		return False

	def has_delete_permission(self, request, obj=None):
		return False

@admin.register(models.HomeNavBarItem)
class HomeNavBarItem(admin.ModelAdmin):
	list_display = ('name', 'url')