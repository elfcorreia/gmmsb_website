from django.db import models
from ckeditor.fields import RichTextField

# Create your models here.

class Page(models.Model):

	slug = models.SlugField(max_length=200)
	title = models.CharField(max_length=200)
	content = RichTextField()
	visible = models.BooleanField(default=True)

	def __str__(self):
		return self.title[:100] + "..."

	class Meta:
		ordering = ["slug"]

class SingletonModel(models.Model):

	class Meta:
		abstract = True

	def save(self, *args, **kwargs):
		self.pk = 1
		super(SingletonModel, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		pass

	@classmethod
	def load(cls):
		obj, created = cls.objects.get_or_create(pk=1)
		return obj

class Home(SingletonModel):
	title = models.CharField(max_length=200)
	content = RichTextField()

	def __str__(self):
		return "Home settings"

class HomeNavBarItem(models.Model):
	name = models.CharField(max_length=200)
	url = models.CharField(max_length=500)

	class Meta:
		ordering = ['name']